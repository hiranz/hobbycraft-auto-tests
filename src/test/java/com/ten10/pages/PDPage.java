package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class PDPage extends PageObject {

    @FindBy(className = "b-button_multi_state")
    private WebElementFacade addToBasket;

    public void addToBasket() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", addToBasket);
//        addToBasket.click();
    }

    @FindBy(className = "b-global_alerts-item")
    private WebElementFacade addedToBasketAlert;

    public void checkProductAddedToBasket() {
        String alert = addedToBasketAlert.getText();
        Assert.assertEquals("Product added to cart", alert);
    }

    @FindBy(linkText = "View Basket")
    private WebElementFacade viewBasketButton;

    public void selectViewBasketButton() {
        viewBasketButton.click();
    }


    @FindBy(css = "div[aria-label=White]")
    private WebElementFacade selectColourFilter;

    @FindBy(id = "//*[@id=\"refinements-panel-title-xl\"]")
    private WebElementFacade panel;

    public void clickColourFilter() {
        waitForCondition().until(ExpectedConditions.elementToBeClickable(selectColourFilter));
        getDriver().findElement(By.xpath("//*[@id='refinements-panel-xl']/div/section[3]/div/div/ul/li[position()=2]/div/span[position()=2]")).click();
//        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
//        executor.executeScript("arguments[0].scrollIntoView();", selectColourFilter);
//        executor.executeScript("arguments[0].click();", selectColourFilter);
    }

    @FindBy(className = "b-applied_filters-value")
    private WebElementFacade checkFilterIsApplied;

    public void checkFilterIsApplied(){
        String filterName = checkFilterIsApplied.getText();
        Assert.assertEquals("White", filterName);
    }

    @FindBy(xpath = "//*[@id=\"product-carousel\"]/section/div/div/div/div[1]/section/div[2]/button")
    private WebElementFacade addToWishlist;

    public void addToWishlist(){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", addToWishlist);
        executor.executeScript("arguments[0].click();", addToWishlist);
    }

    @FindBy(className = "b-global_alerts-item")
    private WebElementFacade alteredWishlistAlert;

    public void addedToWishlistAlert(){
        String alert = alteredWishlistAlert.getText();
        Assert.assertEquals("This product is added to your wishlist", alert);
    }

    public void removedFromWishlistAlert(){
        String alert = alteredWishlistAlert.getText();
        Assert.assertEquals("This product has been removed from your wishlist", alert);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div/main/div[1]/section[2]/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/button[2]")
    private WebElementFacade increaseQuantity;

    public void increaseQuantity(String number) {
        int numberInt = Integer.parseInt(number);
        for(int i=1; i<(numberInt); i++){
            JavascriptExecutor executor = (JavascriptExecutor) getDriver();
            executor.executeScript("arguments[0].click();", increaseQuantity);
        }
    }

    @FindBy(className = "b-global_alerts-item")
    private WebElementFacade quantityIncreased;

    public void checkIncreaseQuantity() {
        String alert = quantityIncreased.getText();
        Assert.assertEquals("Quantity changed", alert);
    }

    @FindBy(className = "b-product_wishlist-btn")
    private WebElementFacade addToWishlistButton;

    public void addToWishlistButton(){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", addToWishlistButton);
    }

    @FindBy(className = "b-header_wishlist-link")
    private WebElementFacade clickOnWishlistPage;

    public void clickOnWishlistPage(){
        clickOnWishlistPage.click();
    }

    @FindBy(className = "b-wishlist-items_count")
    private WebElementFacade checkItemInWishlistPage;

    public void checkItemInWishlistPage() {
        String alert = checkItemInWishlistPage.getText();
        Assert.assertEquals("1 Item", alert);
    }

    @FindBy(id = "dwfrm_evoucher_yourName")
    private WebElementFacade enterYourName;

    @FindBy(id = "dwfrm_evoucher_recepientName")
    private WebElementFacade enterRecipientName;

    @FindBy(id = "dwfrm_evoucher_recepientEmail")
    private WebElementFacade enterRecipientEmail;

    @FindBy(id = "dwfrm_evoucher_confirmRecepientEmail")
    private WebElementFacade confirmRecipientEmail;

    @FindBy(id = "dwfrm_evoucher_sendDate")
    private WebElementFacade enterDate;

    @FindBy(id = "dwfrm_evoucher_message")
    private WebElementFacade enterMessage;

    @FindBy(id = "eproduct-amounts-select")
    private WebElementFacade voucherAmountDropdown;

    @FindBy(xpath = "//*[@id=\"customSelect-wVdCz-panel\"]/div[6]")
    private WebElementFacade voucherAmount;


    public void evoucherDetails(String name, String recipientName, String recipientEmail, String date, String message){

        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        Select dropdown = new Select(getDriver().findElement(By.id("eproduct-amounts-select")));
        dropdown.selectByVisibleText("£10.00");
        executor.executeScript("arguments[0].click();", enterYourName);
        enterYourName.sendKeys(name);
        executor.executeScript("arguments[0].click();", enterRecipientName);
        enterRecipientName.sendKeys(recipientName);
        executor.executeScript("arguments[0].click();", enterRecipientEmail);
        enterRecipientEmail.sendKeys(recipientEmail);
        executor.executeScript("arguments[0].click();", confirmRecipientEmail);
        confirmRecipientEmail.sendKeys(recipientEmail);
        executor.executeScript("arguments[0].click();", enterDate);
        enterDate.sendKeys(date);
        Select dropdown2 = new Select(getDriver().findElement(By.id("dwfrm_evoucher_subject")));
        dropdown2.selectByVisibleText("Merry Christmas! | e-Voucher");
        executor.executeScript("arguments[0].click();", enterMessage);
        enterMessage.sendKeys(message);

    }

    @FindBy(className = "b-minicart-title")
    private WebElementFacade itemsInBasketHeader;

    public void checkNumberItemsInBasket() {
        String alert = itemsInBasketHeader.getText();
        Assert.assertEquals("7 items in basket", alert);
    }

}
