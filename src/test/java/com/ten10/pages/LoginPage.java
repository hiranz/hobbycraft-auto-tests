package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.Random;

public class LoginPage extends PageObject {

    @FindBy(id = "dwfrm_login_email")
    private WebElementFacade emailAddressBox;

    @FindBy(id = "dwfrm_login_password")
    private WebElementFacade passwordBox;

       public void login(String username, String password) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", emailAddressBox);
        emailAddressBox.sendKeys(username);
        executor.executeScript("arguments[0].click()", passwordBox);
        passwordBox.sendKeys(password);
    }

    @FindBy(xpath = "//*[@id=\"login-tab-panel\"]/form/button")
    private WebElementFacade signInButton;

       public void clickSignInButton() {
           JavascriptExecutor executor = (JavascriptExecutor) getDriver();
           executor.executeScript("arguments[0].click()", signInButton);
       }

    @FindBy(id = "button-register")
    private WebElementFacade createAccountButton;

    public void selectCreateAccount() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", createAccountButton);
    }

    @FindBy(id = "dwfrm_profile_customer_firstname")
    private WebElementFacade firstNameBox;

    @FindBy(id = "dwfrm_profile_customer_lastname")
    private WebElementFacade lastNameBox;

    @FindBy(id = "dwfrm_profile_customer_phone")
    private WebElementFacade phoneNumberBox;

    @FindBy(id = "dwfrm_profile_customer_email")
    private WebElementFacade newEmailBox;

    @FindBy(id = "dwfrm_profile_customer_emailconfirm")
    private WebElementFacade confirmEmailBox;

    @FindBy(id = "dwfrm_profile_login_password")
    private WebElementFacade newPasswordBox;

    @FindBy(id = "dwfrm_profile_login_passwordconfirm")
    private WebElementFacade confirmPasswordBox;

    @FindBy(xpath = "//*[@id=\"register-tab-panel\"]/div/form/div[13]/div[1]/label")
    private WebElementFacade acceptTermsAndConditionsBox;

    @FindBy(xpath = "//*[@id=\"register-tab-panel\"]/div/form/div[14]/button")
    private WebElementFacade submitButton;

    @FindBy(id = "dwfrm_profile_customer_agreeToPrivacy-error")
    private WebElementFacade acceptTermsAndConditionsError;

    public void createAccountFormFill(String first_name, String last_name, String phone_number, String email_address, String confirm_email_address, String password, String confirm_password) {

        Random randomGenerator = new Random();
        int randomInt = randomGenerator. nextInt(1000);

        String generatedEmail = email_address + randomInt + "@ten10.com";

        firstNameBox.sendKeys(first_name);
        lastNameBox.sendKeys(last_name);
        phoneNumberBox.sendKeys(String.valueOf(phone_number));
        newEmailBox.sendKeys(generatedEmail);
        confirmEmailBox.sendKeys(generatedEmail);
        newPasswordBox.sendKeys(password);
        confirmPasswordBox.sendKeys(confirm_password);
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", acceptTermsAndConditionsBox);
        executor.executeScript("arguments[0].click();", submitButton);
        }

    @FindBy(className = "b-dialog-title")
    private WebElementFacade successfulAccountMessage;

    public void checkAccountCreated() {
        String alert = successfulAccountMessage.getText();
        Assert.assertEquals("Success!", alert);
    }

    @FindBy(id = "password-reset")
    private WebElementFacade forgotPasswordButton;

    @FindBy(id = "dwfrm_profile_resetPassword_email")
    private WebElementFacade forgotPasswordEmailBox;

    @FindBy(className = "b-reset_password-submit")
    private WebElementFacade resetPassword;

    public void forgotPassword(String email){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", forgotPasswordButton);
        forgotPasswordEmailBox.sendKeys(email);
        executor.executeScript("arguments[0].click();", resetPassword);
    }

    @FindBy(id = "forget-password-form-title")
    private WebElementFacade resetPasswordSuccess;

    public void resetPasswordSuccess(){
    waitForCondition().until(ExpectedConditions.textToBePresentInElement(resetPasswordSuccess, "Success!"));
        String alert = resetPasswordSuccess.getText();
        Assert.assertEquals("Success!", alert);
    }


    public void createAccountFormFillApi(String first_name, String last_name, String phone_number, String email_address, String email_address1, String password, String confirm_password) {
        firstNameBox.sendKeys(first_name);
        lastNameBox.sendKeys(last_name);
        phoneNumberBox.sendKeys(String.valueOf(phone_number));
        newEmailBox.sendKeys(email_address);
        confirmEmailBox.sendKeys(email_address);
        newPasswordBox.sendKeys(password);
        confirmPasswordBox.sendKeys(confirm_password);
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", acceptTermsAndConditionsBox);
        executor.executeScript("arguments[0].click();", submitButton);


    }
}
