package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

@DefaultUrl("https://storefront:Unicorn@bhcg-015.sandbox.us01.dx.commercecloud.salesforce.com/s/hobbycraft-uk/home/")
public class HomePage extends PageObject {

    @FindBy(className = "b-header_login-caption")
    private WebElementFacade signInLink;

    public void clickSignInLink() {
        signInLink.click();
    }

    @FindBy(className = "b-notification_panel-button")
    private WebElementFacade acceptCookiesButton;

    public void clickAcceptCookies() {
        acceptCookiesButton.click();
    }

    @FindBy(className = "b-search_toggle")
    private WebElementFacade searchBarButton;

    public void clickSearchBarButton() {
        searchBarButton.click();
    }

    @FindBy(id = "header-search-input")
    private WebElementFacade searchBar;

    public void searchForProduct(String product) {
        searchBar.sendKeys(product);
    }

    @FindBy(className = "b-search_input-submit")
    private WebElementFacade enterSearchButton;

    public void enterSearch() {
        enterSearchButton.click();
    }

    @FindBy(linkText = "Stores")
    private WebElementFacade clickStores;

    public void clickStores() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", clickStores);
    }

    @FindBy(linkText = "Workshops")
    private WebElementFacade clickWorkshops;

    public void clickWorkshops() {
        clickWorkshops.click();
    }

    @FindBy(linkText = "FAQ")
    private WebElementFacade clickOnFAQs;

    public void clickOnFAQs(){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", clickOnFAQs);
        executor.executeScript("arguments[0].click();", clickOnFAQs);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/main/section/div/h2")
    private WebElementFacade thisWeeksDeals;

    public void checkOnHomePage() {
        String alert = thisWeeksDeals.getText();
        Assert.assertEquals("This Week's Deals", alert);
    }

    @FindBy(xpath = "//*[@id=\"product-carousel\"]/section/div/div/div/div[5]/section/div[2]/button")
    private WebElementFacade wishlistRecommended;

    public void wishlistRecommended(){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", wishlistRecommended);
    }

    @FindBy(xpath = "//*[@id=\"idea-carousel\"]/section/div[2]/div[1]/div/div[2]/section/div[2]/button")
    private WebElementFacade wishListFromIdea;

    public void wishListFromIdea(){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", wishListFromIdea);
    }

    public void chooseCategory(String category, String sub_category) {
        WebElement categoryLink = getDriver().findElement(By.linkText(category));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", categoryLink);
        WebElement subCategoryLink = getDriver().findElement(By.linkText(sub_category));
        executor.executeScript("arguments[0].click();", subCategoryLink);
        executor.executeScript("arguments[0].click();", subCategoryLink);
    }
}

