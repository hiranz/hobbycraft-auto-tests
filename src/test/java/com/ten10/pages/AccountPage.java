package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;

public class AccountPage extends PageObject {

    @FindBy(className = "b-user_greeting-message")
    private WebElementFacade userGreetingMessage;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/main/section[1]/div[2]/ul/li[1]")
    private WebElementFacade profileDetailsName;

    public void checkSuccessfulSignIn(String original_name) {
        String alert = userGreetingMessage.getText();
        Assert.assertEquals("Hi, " + original_name, alert);
    }

    public void checkNameChanged(String name) {
        String newAlert = profileDetailsName.getText();
        Assert.assertEquals("Name: " + name + " Liv", newAlert);
    }

    @FindBy(linkText = "Profile Details")
    private WebElementFacade profileDetails;

    public void selectProfileDetails() {
        profileDetails.click();
    }

    @FindBy(id = "dwfrm_profile_customer_firstname")
    private WebElementFacade firstNameBox;

    public void editName(String original_name) {
        firstNameBox.clear();
        firstNameBox.sendKeys(original_name);
    }

    @FindBy(id = "dwfrm_profile_login_currentpassword")
    private WebElementFacade passwordBox;

    public void enterPassword(String password) {
        passwordBox.sendKeys(password);
    }

    @FindBy(xpath = "//*[@id=\"dwfrm_profile\"]/div[9]/button")
    private WebElementFacade saveChangesButton;

    public void selectSaveChangesButton() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", saveChangesButton);
    }

    @FindBy(linkText = "Payment Methods")
    private WebElementFacade clickPaymentMethods;

    @FindBy(linkText = "Adding a New Payment Card")
    private WebElementFacade addNewCard;

    public void addNewCard() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", clickPaymentMethods);
        executor.executeScript("arguments[0].click();", addNewCard);
    }

    @FindBy(name = "save")
    private WebElementFacade savePayment;

    public void savePayment() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", savePayment);
    }

    @FindBy(className = "b-form-message")
    private WebElementFacade errorMessage;

    public void errorMessage() {
        String newAlert = errorMessage.getText();
        Assert.assertEquals("For technical reasons, your request could not be handled properly at this time. We apologize for any inconvenience.", newAlert);
    }

    @FindBy(linkText = "Address Book")
    private WebElementFacade clickAddressBook;

    @FindBy(linkText = "+ Add A New Address")
    private WebElementFacade addNewAddressLink;

    public void clickAddNewAddress() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", clickAddressBook);
        executor.executeScript("arguments[0].click();", addNewAddressLink);
    }

    @FindBy(id = "dwfrm_address_firstName")
    private WebElementFacade firstName;

    @FindBy(id = "dwfrm_address_lastName")
    private WebElementFacade lastName;

    @FindBy(id = "dwfrm_address_phone")
    private WebElementFacade phoneNumber;

    @FindBy(id = "dwfrm_address_address1")
    private WebElementFacade address1;

    @FindBy(id = "dwfrm_address_address2")
    private WebElementFacade address2;

    @FindBy(id = "dwfrm_address_city")
    private WebElementFacade townBox;

    @FindBy(id = "dwfrm_address_postalCode")
    private WebElementFacade postcodeBox;

    @FindBy(name = "dwfrm_address_apply")
    private WebElementFacade addNewAddress;

    public void addNewAddress(String first_name, String last_name, String phone_number, String address_1, String address_2, String town, String postcode) {

        firstName.sendKeys(first_name);
        lastName.sendKeys(last_name);
        phoneNumber.sendKeys(String.valueOf(phone_number));
        address1.sendKeys(address_1);
        address2.sendKeys(address_2);
        townBox.sendKeys(town);
        postcodeBox.sendKeys(postcode);
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", addNewAddress);
    }

    @FindBy(className = "b-cards_grid-delete")
    private WebElementFacade deleteAddress;

    public void checkAddressSaved() {
        String newAlert = deleteAddress.getText();
        Assert.assertEquals("Delete", newAlert);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/main/div/div[3]/div/div/div[3]/button[1]")
    private WebElementFacade confirmDelete;

    public void deleteAddress() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", deleteAddress);
        executor.executeScript("arguments[0].click();", confirmDelete);
    }

    @FindBy(className = "b-global_alerts-item")
    private WebElementFacade deletedAddressConfirmed;

    public void confirmDeletedAddress() {
        String newAlert = deletedAddressConfirmed.getText();
        Assert.assertEquals("Your address has been deleted. You have no saved addresses", newAlert);
    }

    @FindBy(className = "b-wishlist-title")
    private WebElementFacade wishlistPageName;

    public void wishlistPageName(String wishlistName) {
        String newAlert = wishlistPageName.getText();
        Assert.assertEquals(wishlistName + "'s Wish List", newAlert);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div[2]/main/div/div/div[2]/div/div/div/section/div[2]/div[1]/p/a")
    private WebElementFacade firstWishListItem;

    @FindBy(className = "b-product_wishlist-btn")
    private WebElementFacade removeWishlistButton;

    @FindBy(className = "b-header_wishlist-link")
    private WebElementFacade clickOnWishlistPage;

    @FindBy(className = "b-wishlist-empty_text")
    private WebElementFacade wishListEmpty;

    public void firstWishListItem() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        clickOnWishlistPage.click();
        try {
            for (int i = 1; i < (100); i++) {
                executor.executeScript("arguments[0].click();", firstWishListItem);
                executor.executeScript("arguments[0].click();", removeWishlistButton);
                clickOnWishlistPage.click();
            }
        } catch (Exception e) {
            String newAlert = wishListEmpty.getText();
            Assert.assertEquals("You currently have no items", newAlert);
        }
    }

    @FindBy(linkText = "Change Password")
    private WebElementFacade changePasswordLink;

    @FindBy(id = "dwfrm_changePassword_currentpassword")
    private WebElementFacade currentPasswordBox;

    @FindBy(id = "dwfrm_changePassword_newpassword")
    private WebElementFacade newPasswordBox;

    @FindBy(id = "dwfrm_changePassword_newpasswordconfirm")
    private WebElementFacade confirmNewPasswordBox;

    @FindBy(className = "b-form-btn_save")
    private WebElementFacade changePasswordBox;

    public void changePassword(String currentPassword, String newPassword){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", changePasswordLink);
        currentPasswordBox.sendKeys(currentPassword);
        newPasswordBox.sendKeys(newPassword);
        confirmNewPasswordBox.sendKeys(newPassword);
        executor.executeScript("arguments[0].click();", changePasswordBox);
    }

    @FindBy(xpath = " //*[@id=\"maincontent\"]/div[2]/div/div")
    private WebElementFacade checkPasswordChanged;

    public void checkPasswordChanged(){
        String newAlert = checkPasswordChanged.getText();
        Assert.assertEquals("Your password has been successfully changed", newAlert);
    }
}

