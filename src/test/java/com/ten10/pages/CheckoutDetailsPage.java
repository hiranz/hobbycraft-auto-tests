package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class CheckoutDetailsPage extends PageObject {

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_firstName")
    private WebElementFacade firstNameBox;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_lastName")
    private WebElementFacade lastNameBox;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_phone")
    private WebElementFacade phoneNumberBox;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_address1")
    private WebElementFacade address1Box;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_address2")
    private WebElementFacade address2Box;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_city")
    private WebElementFacade townBox;

    @FindBy(id = "dwfrm_shipping_shippingAddress_addressFields_postalCode")
    private WebElementFacade postcodeBox;

    @FindBy(css = ".b-checkout_step-btn")
    private WebElementFacade saveAndContinue;

    public void deliveryDetailsFormFill(String first_name, String last_name, String phone_number, String address_1, String address_2, String town, String postcode) {

        firstNameBox.sendKeys(first_name);
        lastNameBox.sendKeys(last_name);
        phoneNumberBox.sendKeys(String.valueOf(phone_number));
        address1Box.sendKeys(address_1);
        address2Box.sendKeys(address_2);
        townBox.sendKeys(town);
        postcodeBox.sendKeys(postcode);
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", saveAndContinue);
    }

    @FindBy(xpath = "//*[@id=\"payment-button-adyen\"]/span/span")
    private WebElementFacade payByCard;

    public void clickPayByCard() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", payByCard);
    }

    @FindBy(id = "encryptedCardNumber")
    private WebElementFacade cardNumberBox;

    @FindBy(id = "encryptedExpiryDate")
    private WebElementFacade expiryDateBox;

    @FindBy(id = "encryptedSecurityCode")
    private WebElementFacade securityCodeBox;

    @FindBy(className = "adyen-checkout__card__holderName__input")
    private WebElementFacade cardHolderNameBox;

    @FindBy(xpath = "//*[@id=\"checkout-main\"]/div/div/main/section[2]/div[1]/div[2]/button")
    private WebElementFacade reviewOrderButton;


    public void paymentDetailsFormFill(String card_number, String expiry_date, String CVC, String name) {

        getDriver().switchTo().frame(1);
        cardNumberBox.sendKeys(card_number);
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame(2);
        expiryDateBox.sendKeys(expiry_date);
        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame(3);
        securityCodeBox.sendKeys(CVC);
        getDriver().switchTo().defaultContent();
        cardHolderNameBox.sendKeys(name);
    }

     public void reviewOrder(){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        executor.executeScript("arguments[0].click();", reviewOrderButton);
    }

    @FindBy(id = "dwfrm_login_guestEmail")
    private WebElementFacade checkoutAsGuest;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/main/div/section[2]/form/button")
    private WebElementFacade clickCheckoutAsGuestButton;

    public void checkoutAsGuest(String email) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", checkoutAsGuest);
        checkoutAsGuest.sendKeys(email);
        clickCheckoutAsGuestButton.click();
    }

    @FindBy(xpath = "//*[@id=\"payment-button-PayPal\"]/span/span")
    private WebElementFacade payByPayPal;

    public void payByPal() {
            waitForCondition().until(
           ExpectedConditions.elementToBeClickable(payByPayPal));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", payByPayPal);
    }

    @FindBy(xpath = "//*[@id=\"payment-details-PayPal\"]/div/fieldset/div[1]")
    private WebElementFacade checkPayPalIsSelected;

    public void checkPayPalIsSelected() {
        String alert = checkPayPalIsSelected.getText();
        Assert.assertEquals("Your billing address and phone number will be changed according to the chosen payment method on PayPal side.", alert);
    }

    @FindBy(xpath = "//*[@id=\"buttons-container\"]/div/div/div/div[1]")
    private WebElementFacade selectPayByPayPal;

    public void selectPayByPayPal() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", selectPayByPayPal);
        selectPayByPayPal.click();
    }

    @FindBy(id = "email")
    private WebElementFacade emailPayPalBox;

    @FindBy(id = "btnNext")
    private WebElementFacade nextPayPalButton;

    @FindBy(id = "password")
    private WebElementFacade passwordPayPalBox;

    @FindBy(id = "btnLogin")
    private WebElementFacade loginPayPalButton;

    public void loginToPayPal() {
    emailPayPalBox.sendKeys("daniel.ellison+1@ten10.com");
    nextPayPalButton.click();
    passwordPayPalBox.sendKeys("Password123!");
    loginPayPalButton.click();
    }

    @FindBy(className = "notification-critical")
    private WebElementFacade payPalLoginErrorMessage;

    public void checkPayPalLoginError() {
        String alert = payPalLoginErrorMessage.getText();
        Assert.assertEquals("Some of your information isn't correct. Please try again.", alert);
    }

    @FindBy(xpath = "//*[@id=\"payment-button-GiftCard\"]/span/span[1]")
    private WebElementFacade useGiftCard;

    public void clickUseGiftCard() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", useGiftCard);
    }

    @FindBy(className = "error")
    private WebElementFacade checkGiftCard;

    public void checkGiftCard() {
        String alert = checkGiftCard.getText();
        Assert.assertEquals("Gift Cart or e-Voucher payment service is temporarily not available. Please try again later. If this error continues, please contact Customer Services", alert);
    }

    @FindBy(id = "inputGiftCardNumber")
    private WebElementFacade enterGiftCardNumber;

    @FindBy(id = "inputGiftCardPin")
    private WebElementFacade enterPinNumber;

    @FindBy(xpath = "//*[@id=\"payment-details-GiftCard\"]/div/div/div[1]/button")
    private WebElementFacade useGiftCardButton;

    public void enterGiftCardNumber(String giftCardNumber, String pin) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", enterGiftCardNumber);
        enterGiftCardNumber.sendKeys(giftCardNumber);
        executor.executeScript("arguments[0].click();", enterPinNumber);
        enterPinNumber.sendKeys(pin);
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        executor.executeScript("arguments[0].click();", useGiftCardButton);
    }

    public void collectionDetails(String first_name, String last_name, String phone_number) {

        firstNameBox.sendKeys(first_name);
        lastNameBox.sendKeys(last_name);
        phoneNumberBox.sendKeys(String.valueOf(phone_number));
    }

    public void saveAndContinue(){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", saveAndContinue);
        saveAndContinue.click();
    }

    @FindBy(id = "dwfrm_billing_addressFields_firstName")
    private WebElementFacade firstNameBillingBox;

    @FindBy(id = "dwfrm_billing_addressFields_lastName")
    private WebElementFacade lastNameBillingBox;

    @FindBy(id = "dwfrm_billing_contactInfoFields_phone")
    private WebElementFacade phoneNumberBillingBox;

    @FindBy(id = "dwfrm_billing_addressFields_address1")
    private WebElementFacade address1BillingBox;

    @FindBy(id = "dwfrm_billing_addressFields_address2")
    private WebElementFacade address2BillingBox;

    @FindBy(id = "dwfrm_billing_addressFields_city")
    private WebElementFacade townBillingBox;

    @FindBy(id = "dwfrm_billing_addressFields_postalCode")
    private WebElementFacade postcodeBillingBox;

    @FindBy (xpath = "//*[@id=\"dwfrm_billing_addressFields_country\"]/option[236]")
    private WebElementFacade unitedKingdomCountryOption;

    public void billingDetailsFormFill(String first_name, String last_name, String phone_number, String address_1, String address_2, String town, String postcode) {

        firstNameBillingBox.sendKeys(first_name);
        lastNameBillingBox.sendKeys(last_name);
        phoneNumberBillingBox.sendKeys(String.valueOf(phone_number));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        Select dropdown = new Select(getDriver().findElement(By.id("dwfrm_billing_addressFields_country")));
        dropdown.selectByVisibleText("United Kingdom");
        address1BillingBox.sendKeys(address_1);
        address2BillingBox.sendKeys(address_2);
        townBillingBox.sendKeys(town);
        postcodeBillingBox.sendKeys(postcode);
        waitFor(5000);
    }

    @FindBy(xpath = "//*[@id=\"checkout-main\"]/div/div/main/section[3]/div/div[1]/div/div/div[2]/div[1]/label")
    private WebElementFacade acceptTerms;

    @FindBy(xpath = "//*[@id=\"checkout-main\"]/div/div/main/section[3]/div/div[2]/button/span")
    private WebElementFacade placeOrder;

    @FindBy(xpath = "//*[@id=\"checkout-main\"]/div/div/main/div/div")
    private WebElementFacade assertComplete;

    @FindBy(xpath = "//*[@id=\"dwfrm_billing\"]/div[1]")
    private WebElementFacade giftCardErrorMessage;

    public void completeOrder() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        executor.executeScript("arguments[0].click();", reviewOrderButton);
        executor.executeScript("arguments[0].click();", acceptTerms);
        executor.executeScript("arguments[0].click();", placeOrder);
    }

    public void checkOrderCompleted() {
        String alert = assertComplete.getText();
        Assert.assertEquals("We're sorry that your order could not be placed. This may be due to a high order volume or temporary connection errors. Please wait a few minutes and try again. We won't process your payment until you successfully place your order. If you have further questions, please contact Customer Services.", alert);
    }

    public void checkOrderCompletedForGiftCard() {
        String alert = giftCardErrorMessage.getText();
        Assert.assertEquals("Sorry, the payment you submitted is not valid. Please re-enter payment information. If the issue continues, please contact our Customer Service", alert);
    }

    @FindBy(xpath = "//*[@id=\"dwfrm_billing\"]/div[1]")
    private WebElementFacade paymentDeclined;

    public void paymentDeclined(){
        String alert = paymentDeclined.getText();
        Assert.assertEquals("Payment was declined", alert);
    }

}

// Dan's Version
//    public void completeOrder() {
//        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
//        executor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//        executor.executeScript("arguments[0].click();", reviewOrderButton);
//        executor.executeScript("arguments[0].scrollIntoView();", acceptTerms);
//        acceptTerms.click();
//        executor.executeScript("arguments[0].click();", placeOrder);
//    }