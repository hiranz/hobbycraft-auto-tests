package com.ten10.pages;

import lombok.var;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static java.lang.Float.parseFloat;

public class BasketPage extends PageObject {

    @FindBy(linkText = "Checkout")
    private WebElementFacade checkoutButton;

    public void selectCheckoutButton() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        waitForCondition().until(
                ExpectedConditions.elementToBeClickable(checkoutButton));
        executor.executeScript("arguments[0].click();", checkoutButton);
    }

    @FindBy(xpath = "//*[@id=\"maincontent\"]/main/div/section[1]/form/button")
    private WebElementFacade clickSignInAndCheckoutButton;

    public void selectSignInAndCheckoutButton() {
        clickSignInAndCheckoutButton.click();
    }


    @FindBy(xpath = "//*[@id=\"maincontent\"]/div/div[3]/div/div/div[2]/aside[2]/div/div/div[1]/button[2]")
    private WebElementFacade selectClickAndCollect;

    public void selectClickAndCollect() {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", selectClickAndCollect);
    }

    @FindBy(xpath = "//*[@id=\"clickCollect\"]/section[2]/div[1]/button")
    private WebElementFacade clickFindAStore;

    public void clickFindAStore(){
            waitForCondition().until(
           ExpectedConditions.visibilityOf(clickFindAStore));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", clickFindAStore);
    }


    @FindBy(xpath = "//*[@id=\"maincontent\"]/div/div[3]/div/div/div[2]/main/div/div/p")
    private WebElementFacade checkSpendAlert;

    public void checkSpendAlert() {
        String alert = checkSpendAlert.getText();
        Assert.assertEquals("Minimum spend £10.00 for Click & Collect", alert);
    }

    @FindBy(className = "b-coupon_form-title")
    private WebElementFacade addPromoCode;

    @FindBy(id = "dwfrm_coupon_couponCode")
    private WebElementFacade typePromoCode;

    @FindBy(xpath = "//*[@id=\"coupon-form\"]/form/button")
    private WebElementFacade applyCode;

    public void addPromoCode(String promoCode){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].scrollIntoView();", addPromoCode);
        executor.executeScript("arguments[0].click();", addPromoCode);
        typePromoCode.sendKeys(promoCode);
        executor.executeScript("arguments[0].click();", addPromoCode);
        executor.executeScript("arguments[0].click();", applyCode);
    }

    public void checkCorrectItemsInBasket(String product_1, String product_2){
        WebElement productLink1 = getDriver().findElement(By.linkText(product_1));
        WebElement productLink2 = getDriver().findElement(By.linkText(product_2));
        String product1 = productLink1.getText();
        Assert.assertEquals(product_1, product1);
        String product2 = productLink2.getText();
        Assert.assertEquals(product_2, product2);
    }


    @FindBy(className = "b-cart_product-remove")
    private WebElementFacade removeItem;

    @FindBy(xpath = "//*[@id=\"maincontent\"]/div/div[4]/div/div/div[3]/button[2]")
    private WebElementFacade removeItemButton;

    public void removeItemFromBasket(){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", removeItem);
        executor.executeScript("arguments[0].click();", removeItemButton);
    }

    @FindBy(className = "b-global_alerts-item")
    private WebElementFacade productRemovedAlert;

    public void productRemovedAlert(){
        String alert = productRemovedAlert.getText();
        Assert.assertEquals("Product removed", alert);
    }

}
