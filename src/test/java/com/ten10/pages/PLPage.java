package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class PLPage extends PageObject {

    public void selectProduct(String product) {

        WebElement productLink = getDriver().findElement(By.linkText(product));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", productLink);
    }

    @FindBy(css = "#product-grid > div.l-plp_grid > div:nth-child(2) > section > div.b-product_tile-info > div.b-product_tile-bottom > button")
    private WebElementFacade addProductFromPLP;

    public void addProductFromPLP(){
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", addProductFromPLP);
    }

    @FindBy(linkText = "View Basket")
    private WebElementFacade selectViewBasket;

    public void selectViewBasket(){
        waitForCondition().until(
                ExpectedConditions.visibilityOf(selectViewBasket));
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", selectViewBasket);
    }

    public void priceLowToHigh(){
        Select dropdown = new Select(getDriver().findElement(By.id("plp-sort")));
        dropdown.selectByVisibleText("Price Low To High");
    }

    @FindBy(className = "b-dialog-title")
    private WebElementFacade sortingAppliedError;

    public void sortingApplied(){
        String alert = sortingAppliedError.getText();
        Assert.assertEquals("Oops...", alert);
    }

    @FindBy(className = "b-header_category-title")
    private WebElementFacade subCategoryTitle;

    public void checkCategoryTitle(String sub_category){
        String title = subCategoryTitle.getText();
        Assert.assertEquals(sub_category, title);
    }

}
