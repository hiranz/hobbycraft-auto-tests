package com.ten10.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class MiscellaneousPage extends PageObject {

    @FindBy(className = "b-error_page-title")
    private WebElementFacade pageNotFound;

    public void pageNotFound() {
        String alert = pageNotFound.getText();
        Assert.assertEquals("Page not found", alert);
    }

    @FindBy(xpath = "//*[@id=\"ext-gen44\"]/body/header/div/div[2]/a")
    private WebElementFacade goToHomepage;

    public void goToHomepage(){
        goToHomepage.click();
    }



}


//Useful Code:

// Assert:
//        String alert = webElementFacadeName.getText();
//        Assert.assertEquals("Expected Text", alert);

// Our Pal Javascript:
//    JavascriptExecutor executor = (JavascriptExecutor) getDriver();
//        executor.executeScript("arguments[0].click();", webElementFacadeName);
//        executor.executeScript("arguments[0].scrollIntoView();", webElementFacadeName);

// Wait:
//    waitForCondition().until(ExpectedConditions.visibilityOf(reviewOrderButton));