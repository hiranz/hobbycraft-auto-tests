package com.ten10.stepdefinitions;

import com.ten10.pages.MiscellaneousPage;
import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;

public class MiscellaneousStepDefs {

    @Steps
    MiscellaneousPage miscellaneousPage;

    @And("Check that page not found")
    public void pageNotFound() {
        miscellaneousPage.pageNotFound();
    }

    @And("Click the Hobbycraft logo to return home")
    public void goToHomepage(){
        miscellaneousPage.goToHomepage();
    }


}
