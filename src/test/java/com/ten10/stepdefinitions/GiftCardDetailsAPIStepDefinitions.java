package com.ten10.stepdefinitions;

import com.ten10.apiServices.Endpoints;
import io.cucumber.datatable.DataTable;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class GiftCardDetailsAPIStepDefinitions {


    @Steps
    Endpoints endpoints;


    @And("create user response should be valid")
    public void createUserResponseShouldBeValid() {
    }

    @Given("the following gift card details:")
    public void theFollowingGiftCardDetails(DataTable table) {
        List<Map<String, String>> data  = table.asMaps(String.class, String.class);
        endpoints.makeRequest(data);
    }

    @When("I ask for the gift card details")
    public void iAskForTheGiftCardDetails() {
        endpoints.triggerRequest();
    }

    @Then("the fetched result should include the following details:")
    public void theFetchedResultShouldIncludeTheFollowingDetails(List<Map<String, String>> giftCardDetails) {
        Map<String, String> expectedResponse = giftCardDetails.get(0);
        Map<String, String>actualResponse =  endpoints.returnResponseBody();
        assertThat(actualResponse).containsAllEntriesOf(expectedResponse);
    }
}
