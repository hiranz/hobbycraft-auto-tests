package com.ten10.stepdefinitions;

import com.ten10.pages.BasketPage;
import com.ten10.pages.PDPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class PDPStepDefs {

    @Steps
    PDPage pDPage;

    @Steps
    BasketPage basketPage;

    @And("Add the item to basket from PDP")
    public void addItemToBasket() {
        pDPage.addToBasket();
    }

    @And("Add increased quantity to basket from PDP")
    public void addIncreasedQuantityToBasket() {
        pDPage.addToBasket();
    }

    @Then("The 'Product added to basket' alert should appear")
    public void checkItemAddedToBasket() {
        pDPage.checkProductAddedToBasket();
    }

    @And("Select view basket for home delivery")
    public void selectViewBasket() {
        pDPage.selectViewBasketButton();
    }

    @And("Select view basket for click and collect")
    public void selectViewBasketForClickAndCollect() {
        pDPage.selectViewBasketButton();
        basketPage.selectClickAndCollect();
    }

    @And("Apply colour filter")
    public void applyColourFilter() {
        pDPage.clickColourFilter();
    }

    @Then("Check filter is successfully applied")
    public void checkFilterIsApplied() {
        pDPage.checkFilterIsApplied();
    }

    @And("Add recently viewed item to wishlist")
    public void addItemToWishlist() {
        pDPage.addToWishlist();
    }

    @Then("Check item has been added to wishlist")
    public void checkItemIsAddedToWishlist() {
        pDPage.addedToWishlistAlert();
    }


    @And("Increase product quantity with {string}")
    public void increaseProductQuantity(String number) {
        pDPage.increaseQuantity(number);
        pDPage.checkIncreaseQuantity();
    }

    @And("Check that quantity has increased")
    public void checkQuantityIncreased(){
        pDPage.checkIncreaseQuantity();
    }

    @And("Add product to wishlist")
    public void addToWishlistButton(){
        pDPage.addToWishlistButton();
        pDPage.addedToWishlistAlert();
    }

    @And("Navigate to the wishlist page")
    public void clickOnWishlistPage(){
        pDPage.clickOnWishlistPage();
    }

    @Then("Check item is in wishlist page")
    public void checkItemIsInWishlistPage(){
        pDPage.checkItemInWishlistPage();
    }

    @And("Fill out e-voucher details with {string}, {string}, {string}, {string} and {string}")
    public void evoucherDetails(String name, String recipientName, String recipientEmail, String date, String message){
        pDPage.evoucherDetails(name, recipientName, recipientEmail, date, message);
    }

    @Then("Check correct number of items in basket")
    public void checkNumberItemsInBasket(){
        pDPage.checkNumberItemsInBasket();
    }

}
