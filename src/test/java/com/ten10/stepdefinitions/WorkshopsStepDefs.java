package com.ten10.stepdefinitions;

import com.ten10.pages.WorkshopsPage;
import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;

public class WorkshopsStepDefs {

    @Steps
    WorkshopsPage workshopsPage;

    @And("I type in my {string}")
    public void typeInPostcode(String postcode) {
        workshopsPage.typeInPostcode(postcode);
    }

    @And("I choose a craft type")
    public void chooseCraftType() {
        workshopsPage.chooseCraftType();
    }

    @And("Apply filters")
    public void applyFilters() {
        workshopsPage.applyFilters();
    }

    @And("Choose a workshop")
    public void chooseWorkshop() {
        workshopsPage.chooseWorkshop();
    }

    @And("Book a slot")
    public void bookASlot() {
        workshopsPage.chooseASlot();
    }

}