package com.ten10.stepdefinitions;

import com.ten10.pages.AccountPage;
import com.ten10.pages.CheckoutDetailsPage;
import com.ten10.pages.PDPage;
import com.ten10.pages.PLPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en_scouse.An;
import net.thucydides.core.annotations.Steps;

public class AccountStepDefs {

    @Steps
    AccountPage accountPage;

    @Steps
    CheckoutDetailsPage checkoutDetailsPage;

    @Steps
    PLPage plPage;

    @Steps
    PDPage pDPage;

    @Then("Check I am signed in as {string}")
    public void checkSuccessfulSignIn(String original_name) {
        accountPage.checkSuccessfulSignIn(original_name);
    }

    @Then("Check name has been successfully changed to {string}")
    public void checkNameHasChanged(String name) {
        accountPage.checkNameChanged(name);
    }

    @And("To edit {string} enter {string}")
    public void editNameChange(String original_name, String password) {
        accountPage.selectProfileDetails();
        accountPage.editName(original_name);
        accountPage.enterPassword(password);
        accountPage.selectSaveChangesButton();
    }

    @And("Add new card with {string}, {string}, {string} and {string}")
    public void addNewCard(String card_number, String expiry_date, String CVC, String name){
        accountPage.addNewCard();
        checkoutDetailsPage.paymentDetailsFormFill(card_number, expiry_date, CVC, name);
        accountPage.savePayment();
    }

    @And("Check card error message appears")
    public void checkCardError(){
        accountPage.errorMessage();
    }

    @And("Add new address with {string}, {string}, {string}, {string}, {string}, {string} and {string}")
    public void addNewAddress(String first_name, String last_name, String phone_number, String address_1, String address_2, String town, String postcode){
        accountPage.clickAddNewAddress();
        accountPage.addNewAddress(first_name, last_name, phone_number, address_1, address_2, town, postcode);
    }

    @Then("Check address saved")
    public void checkAddressSaved(){
        accountPage.checkAddressSaved();
    }

    @And("Delete address")
    public void deleteAddress(){
        accountPage.deleteAddress();
    }

    @Then("Check address has been deleted")
    public void checkAddressDeleted(){
        accountPage.confirmDeletedAddress();
    }

    @Then("Check successfully navigated to wishlist page with {string}")
    public void wishlistPage(String wishListName){
        accountPage.wishlistPageName(wishListName);
    }

    @And("Clear {string} and {string} from wishlist")
    public void clearWishlistPage(String product_1, String product_2){
        plPage.selectProduct(product_1);
        pDPage.addToWishlistButton();
        pDPage.removedFromWishlistAlert();
        pDPage.clickOnWishlistPage();
        plPage.selectProduct(product_2);
        pDPage.addToWishlistButton();
        pDPage.removedFromWishlistAlert();
    }

    @And("Ensure wishlist is clear")
    public void clearWishlist(){
        accountPage.firstWishListItem();
    }

    @And("Change from {string} to {string}")
    public void changePassword(String currentPassword, String newPassword){
        accountPage.changePassword(currentPassword, newPassword);
    }

    @Then("Check password successfully changed")
    public void checkPasswordChange(){
        accountPage.checkPasswordChanged();
    }



}

