package com.ten10.stepdefinitions;
import com.ten10.pages.BasketPage;
import com.ten10.pages.CheckoutDetailsPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class CheckoutDetailsStepDefs {

    @Steps
    CheckoutDetailsPage checkoutDetailsPage;

    @Steps
    BasketPage basketPage;

    @And("Fill in delivery details with {string}, {string}, {string}, {string}, {string}, {string} and {string}")
    public void fillInDeliveryDetails(String first_name, String last_name, String phone_number, String address_1, String address_2, String town, String postcode) {
        checkoutDetailsPage.deliveryDetailsFormFill(first_name, last_name, phone_number, address_1, address_2, town, postcode);
    }

    @And("Fill in payment details with {string}, {string}, {string}, {string}")
    public void fillInPaymentDetails(String card_number, String expiry_date, String CVC, String name) {
        checkoutDetailsPage.clickPayByCard();
        checkoutDetailsPage.paymentDetailsFormFill(card_number, expiry_date, CVC, name);
        checkoutDetailsPage.reviewOrder();
    }

    @And("Checkout as guest with {string}")
    public void checkoutAsGuest(String email){
        basketPage.selectCheckoutButton();
        checkoutDetailsPage.checkoutAsGuest(email);
    }

    @And("Click pay by PayPal")
    public void payByPayPal(){
        checkoutDetailsPage.payByPal();

    }

    @Then("Select pay by PayPal")
    public void checkPayPalIsSelected(){
        checkoutDetailsPage.payByPal();
        checkoutDetailsPage.checkPayPalIsSelected();
    }

    @And("Login to PayPal")
    public void loginToPayPal(){
        checkoutDetailsPage.selectPayByPayPal();
        checkoutDetailsPage.loginToPayPal();
        checkoutDetailsPage.checkPayPalLoginError();
    }

    @And("Fill in gift card details with {string} and {string}")
    public void enterGiftCardNumber(String giftCardNumber, String pin){
        checkoutDetailsPage.clickUseGiftCard();
        checkoutDetailsPage.enterGiftCardNumber(giftCardNumber, pin);
        checkoutDetailsPage.checkGiftCard();
    }

    @And("Fill in collection details with {string}, {string} and {string}")
    public void collectionDetails(String first_name, String last_name, String phone_number){
        checkoutDetailsPage.collectionDetails(first_name, last_name, phone_number);

    }

    @And("Fill in billing details with {string}, {string}, {string}, {string}, {string}, {string} and {string}")
    public void billingDetails(String first_name, String last_name, String mobile_number, String address_1, String address_2, String town, String postcode){
        checkoutDetailsPage.billingDetailsFormFill(first_name, last_name, mobile_number,address_1, address_2, town, postcode);
    }

    @Then("Payment was declined")
    public void paymentDeclined(){
        checkoutDetailsPage.paymentDeclined();
    }

    @And("Click save and continue")
    public void saveAndContinue(){
        checkoutDetailsPage.saveAndContinue();
    }

    @Then("Complete order")
    public void completeOrder(){
        checkoutDetailsPage.completeOrder();
        checkoutDetailsPage.checkOrderCompleted();
    }

    @Then("Complete order for gift card")
    public void completeOrderForGiftCard(){
        checkoutDetailsPage.completeOrder();
        checkoutDetailsPage.checkOrderCompletedForGiftCard();
    }
}

