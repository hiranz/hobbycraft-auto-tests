package com.ten10.stepdefinitions;

import com.ten10.apiServices.TestEmailAccountAPI;
import com.ten10.navigation.NavigateTo;
import com.ten10.pages.AccountPage;
import com.ten10.pages.HomePage;
import com.ten10.pages.LoginPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;

public class LoginStepDefs {

    String email_Address;
    Response emailAccountResponse;

    @Steps
    HomePage homePage;

    @Steps
    LoginPage loginPage;

    @Steps
    TestEmailAccountAPI testEmailAccountAPI ;

    @Steps
    NavigateTo navigateTo;

    @Steps
    AccountPage accountPage;


    @When("I sign in with {string} and {string}")
    public void signIn(String username, String password) {
        homePage.clickSignInLink();
        loginPage.login(username, password);
        loginPage.clickSignInButton();
    }

    @When("I create an account with {string}, {string}, {string}, {string}, {string}, {string} and {string}")
    public void fillInRegistrationForm(String first_name, String last_name, String phone_number, String email_address, String confirm_email_address, String password, String confirm_password) {
        homePage.clickSignInLink();
        loginPage.selectCreateAccount();
        loginPage.createAccountFormFill(first_name, last_name, phone_number, email_address, confirm_email_address, password, confirm_password);
    }

    @Then("Check account is successfully created")
    public void checkAccountCreated() {
        loginPage.checkAccountCreated();
    }

    @When("I click forgot password with {string}")
    public void forgotPassword(String email) {
        homePage.clickSignInLink();
        loginPage.forgotPassword(email);
    }

    @Then("Check forgot password email pop up appears")
    public void checkPasswordResetEmailPopUp(){
        loginPage.resetPasswordSuccess();
    }

    @Given("an user account is created  with prefix {string}")
    public void anUserAccountIsCreatedWithPrefix(String emailIdPrefix) {
        Response emailAccountResponse  =testEmailAccountAPI.createEmailAccount(emailIdPrefix);
        email_Address = testEmailAccountAPI.getEmail_Id();
    }

    @When("I create an account with {string}, {string}, {string}, {string} and {string}")
    public void iCreateAnAccountWithFirst_nameLast_namePhone_numberPasswordAndConfirm_password(String first_name, String last_name, String phone_number, String password, String confirm_password) {
        homePage.clickSignInLink();
        loginPage.selectCreateAccount();
        loginPage.createAccountFormFillApi(first_name, last_name, phone_number, email_Address, email_Address, password, confirm_password);
        Response responseLink = testEmailAccountAPI.getMail();
        String activation_link = testEmailAccountAPI.getAccountActivationLink(responseLink);
        navigateTo.activationPage(activation_link);
        loginPage.login(email_Address, password);
        loginPage.clickSignInButton();
        accountPage.checkSuccessfulSignIn(first_name);
    }

}
