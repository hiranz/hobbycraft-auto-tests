package com.ten10.apiServices;
import io.restassured.filter.log.*;
import com.ten10.utils.EmailGenerator;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;


public class EmailAccountEndpoint extends PageObject {

    private final String EMAIL_ACCOUNT_BASE_URI = "https://temp-email.stub.me.uk";

    private final String TOKEN_CREATION_ENDPOINT = "/api/token";

    private final String EMAIL_INBOX_CREATION_ENDPOINT = "api/create/HCA/{email_Id}";

    private final String EMAIL_INBOX_ENDPOINT = "/api/token/{email_Id}";

    private final String EMAIL_INBOX_MAIL_ENDPOINT = "/api/inbox/{email_Id}/1";

    public String getEmail_Id() {
        return email_Id;
    }

    private String email_Id;

    public String getTOKEN_CREATION_ENDPOINT() {
        return this.TOKEN_CREATION_ENDPOINT;
    }

    public String getEMAIL_INBOX_CREATION_ENDPOINT() {
        return this.EMAIL_INBOX_CREATION_ENDPOINT;
    }

    public String getEMAIL_INBOX_ENDPOINT() {
        return this.EMAIL_INBOX_ENDPOINT;
    }

    public String getEMAIL_INBOX_MAIL_ENDPOINT() {
        return this.EMAIL_INBOX_MAIL_ENDPOINT;
    }

    public String getEMAIL_ACCOUNT_BASE_URI() {
        return this.EMAIL_ACCOUNT_BASE_URI;
    }

    public Response getToken() {

        return SerenityRest.given()
                .filter(new ResponseLoggingFilter())
                .filter(new RequestLoggingFilter())
                .baseUri(this.EMAIL_ACCOUNT_BASE_URI)
                .formParam("username", "ten10")
                .formParam("password", "secret")
                .formParam("grant_type", "password")
                .post(this.getTOKEN_CREATION_ENDPOINT());
        //String bearer_token = response.jsonPath().get("access_token");
    }

    public Response createEmailAccount(String emailIdPrefix) {
        Response responseGetToken = this.getToken();
        String bearer_token = responseGetToken.jsonPath().get("access_token");
         this.email_Id = EmailGenerator.generateEmail(emailIdPrefix);
        return  SerenityRest.given().baseUri(this.EMAIL_ACCOUNT_BASE_URI)
                .filter(new ResponseLoggingFilter())
                .filter(new RequestLoggingFilter())
                .pathParam("email_Id", email_Id)
                .header("Authorization", "Bearer " + bearer_token)
                .post(this.EMAIL_INBOX_CREATION_ENDPOINT);

    }

     public Response getMail() {
         File jsonDataInFile = new File("src/test/resources/payloads/accountactivate.json");
     //    Response responseEmailAccount = this.createEmailAccount();

         return RestAssured.given()
                 .filter(new ResponseLoggingFilter())
                 .filter(new RequestLoggingFilter())
                 .baseUri(this.getEMAIL_ACCOUNT_BASE_URI())
                 .pathParam("email_Id", email_Id )
                 .header("Authorization", "Bearer " + this.getToken())
                 .contentType(ContentType.JSON)
                 .body(jsonDataInFile)
                 .post(this.EMAIL_INBOX_MAIL_ENDPOINT);


    }

      public String getAccountActivationLink(){

          Response responseEmail = this.getMail();

          String accountlink = responseEmail.getBody().jsonPath().get("results[0]");

          return accountlink;
      }


}
