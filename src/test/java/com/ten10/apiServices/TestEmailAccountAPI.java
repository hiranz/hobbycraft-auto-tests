package com.ten10.apiServices;

import com.ten10.utils.EmailGenerator;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.rest.SerenityRest;

import java.io.File;
import java.util.HashMap;
import java.util.Map;


public class TestEmailAccountAPI extends PageObject {


    private final String TOKEN_CREATION_ENDPOINT = "https://temp-email.stub.me.uk/api/token";

    private final String EMAIL_INBOX_CREATION_ENDPOINT = "https://temp-email.stub.me.uk/api/create/HCA/{email_Id}";


    private final String EMAIL_INBOX_MAIL_ENDPOINT = "https://temp-email.stub.me.uk/api/inbox/{email_Id}/1";


    public String getEmail_Id() {
        return email_Id;
    }

    private String email_Id;


    public Response getToken() {

        return SerenityRest.given()
                .filter(new ResponseLoggingFilter())
                .filter(new RequestLoggingFilter())
                .formParam("username", "ten10")
                .formParam("password", "secret")
                .formParam("grant_type", "password")
                .post(TOKEN_CREATION_ENDPOINT);
        //String bearer_token = response.jsonPath().get("access_token");
    }

    public Response createEmailAccount(String emailIdPrefix) {
        Response responseGetToken = this.getToken();
        String bearer_token = responseGetToken.jsonPath().get("access_token");
         this.email_Id = EmailGenerator.generateEmail(emailIdPrefix);
        return  SerenityRest.given()
                .filter(new ResponseLoggingFilter())
                .filter(new RequestLoggingFilter())
                .pathParam("email_Id", email_Id)
                .header("Authorization", "Bearer " + bearer_token)
                .post(EMAIL_INBOX_CREATION_ENDPOINT);

    }

     public Response getMail() {
         File jsonDataInFile = new File("src/test/resources/payloads/accountactivate.json");
//       Response responseEmailAccount = this.createEmailAccount(this.getEmail_Id());
         Response responseGetToken = this.getToken();
         String bearer_token = responseGetToken.jsonPath().get("access_token");
         Map<String, Object> map = new HashMap<String, Object>();
         map.put("xpath", "//a[contains(text(), 'Activate My Account')]/@href");
         map.put("fail_no_exist", true);
         return SerenityRest.given()
                 .filter(new ResponseLoggingFilter())
                 .filter(new RequestLoggingFilter())
                 .pathParam("email_Id", email_Id )
                 .header("Authorization", "Bearer " + bearer_token)
                 .contentType(ContentType.JSON)
                 .body(map)
                 .post(EMAIL_INBOX_MAIL_ENDPOINT);


    }

      public String getAccountActivationLink(Response response){

          Response responseEmail = response;

          String accountlink = responseEmail.getBody().jsonPath().get("results[0]");

          return accountlink;
      }


}
