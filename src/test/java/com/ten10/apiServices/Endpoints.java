package com.ten10.apiServices;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;
import com.ten10.utils.EmailGenerator;
import org.openqa.selenium.JavascriptExecutor;

import javax.net.ssl.HttpsURLConnection;


public class Endpoints extends PageObject {

    private static EnvironmentVariables environmentVariables;
    private static String  GIFT_CARD_URI = "/gift-cards/{giftcardnumber}";



    public RequestSpecification getRequestSpecification() {
        return requestSpecification;
    }
    private RequestSpecification requestSpecification;

    public Response getResponse() {
        return response;
    }
    private Response response;

    public Map<String, String> returnResponseBody(){
        return mapOfStringsFrom(getResponse().getBody().as(Map.class));
    }

    private Map<String,String> mapOfStringsFrom(Map<String, Object> map) {
        return map.entrySet()
                .stream()
                .collect(toMap(Map.Entry::getKey,
                        entry -> entry.getValue().toString()));
    }


    public static Response getGC(String giftCardNum, String securityCode, String Authorization, String coRelation_id) {

        String baseURL =  EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("api.base.url");


        RequestSpecification request = SerenityRest.given().baseUri(baseURL);

        request.header("Content-Type", "application/json");
        Response response = request.relaxedHTTPSValidation()
                .pathParam("giftcardnumber", giftCardNum)
                .header("securityCode", securityCode)
                .header("Authorization", Authorization)
                .header("coRelation_id", coRelation_id).get("/gift-cards/{giftcardnumber}");
        return response;
    }


    public void makeRequest(List<Map<String, String>> giftCarddata) {
        String baseURL =  EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("external.api.base.url");

        RequestSpecification request = SerenityRest.given().baseUri(baseURL)
                .relaxedHTTPSValidation()
                .pathParam("giftcardnumber", giftCarddata.get(0).get("Gift Card Number"))
                .header("securityCode", giftCarddata.get(0).get("Security Code"))
                .header("Authorization", giftCarddata.get(0).get("Authorization"))
                .header("x-correlation-id", giftCarddata.get(0).get("x-correlation-id"));

        this.requestSpecification=  request;

    }

    public void  triggerRequest() {
        this.response = getRequestSpecification().get(GIFT_CARD_URI);
    }

    public void createAccount() throws IOException {

        //Create a Token
        Response response = RestAssured.given()
                .formParam("username","ten10")
                .formParam("password","secret")
                .formParam("grant_type", "password")
                .post("https://temp-email.stub.me.uk/api/token");
        String bearer_token = response.jsonPath().get("access_token");



       //Using the token create an user email account
//       String email_Id = EmailGenerator.generateEmail();
        String email_Id = "HCAuto_04e23_1637159799@stub.me.uk";
        System.out.println(email_Id);
        Response create_user_response  = RestAssured.given().baseUri("https://temp-email.stub.me.uk/api/create/HCA")
                .pathParam("email_Id", email_Id )
                .header("Authorization", "Bearer " + bearer_token)
                .post("/{email_Id}");
        create_user_response.prettyPrint();

        //Verify that an email is present in Inbox -Options
        Response response3 = RestAssured.given()
                .header("Authorization", "Bearer " + bearer_token)
                .pathParam("email_Id", email_Id )
                .get("https://temp-email.stub.me.uk/api/inbox/{email_Id}");
                response3.prettyPrint();

        //Click on the Account Activate Link in Inbox
        File jsonDataInFile = new File("src/test/resources/payloads/accountactivate.json");

        Response response4 = RestAssured.given()
                .baseUri("https://temp-email.stub.me.uk/api/inbox")
                .pathParam("email_Id", email_Id )
                .header("Authorization", "Bearer " + bearer_token)
                .contentType(ContentType.JSON)
                .body(jsonDataInFile)
                .post("/{email_Id}/1");

        String accountlink = response4.getBody().jsonPath().get("results[0]");


       /* URL url = new URL("https://bhcg-015.sandbox.us01.dx.commercecloud.salesforce.com/on/demandware.store/Sites-hobbycraft-uk-Site/en_GB/Account-ActivateProfile?activationToken=rNPLOqKFKIJ8VnA52ziBvPyFLeZ4VbLyXnrYLVM%2b1ZnHxW7i4Zvutf2o1pnOZUNtvKPg3OCBbMPmrPE4Vyz7L0FPy0JtKA%2bmRzHP6FG2EChLV3fNVgQmVEOLEpfdBcre");
        HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();
        connection.setRequestMethod("GET");*/

//        Response respone_accountactivation1 = RestAssured.given()
//                .auth()
//                .preemptive()
//                .basic("storefront","Unicorn")
//                .contentType(ContentType.URLENC)
//                .redirects().follow(false)
//                .baseUri(accountlink)
//                .relaxedHTTPSValidation()
//                .get();
//        String headerLocationValue = respone_accountactivation1.getHeader("Location");

//        getDriver().navigate().to(accountlink);

//        Response finalone =
//                RestAssured.given()
//                         .auth()
//                .preemptive()
//                .basic("storefront","Unicorn")
//                .contentType(ContentType.URLENC)
//                .redirects().follow(true)
//                .baseUri(accountlink)
//                .relaxedHTTPSValidation()
//                        .get(accountlink);

        getDriver().navigate().to(accountlink);

//        System.out.println(respone_accountactivation.statusCode());
//        System.out.println(respone_accountactivation.getHeaders());
//        System.out.println(respone_accountactivation.prettyPrint());


    //Create an Account
    //Register at HC with that Account
    //Retrieve the account activation link




    }
}
