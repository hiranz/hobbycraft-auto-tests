Feature: Fetch a specific gift card details & balance  Gift card balance check call from SFCC to NAV

  @api
  Scenario: Fetch specific gift card details & balance using valid Card Number
    Given the following gift card details:
      | Gift Card Number | Security Code | Authorization                        | x-correlation-id                     |
      | 9876540831358278 | ODY4          | 421cb368-066b-11e1-9647-c5be7398b24e | 421cb368-066b-11e1-9647-c5be7398b24e |
    When I ask for the gift card details
    Then the fetched result should include the following details:
      | cardNumber       | cardType  | status | amount |
      | 9876540831358278 | GIFTCARDS | valid  | 12.2   |